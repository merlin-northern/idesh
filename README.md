idesh - IDE in Shell

The idea here is to provide typical IDE (Integrated Development Environment) functionality
with basic UNIX tools and commands adding only a layer of simple scripts to make things
easier.

Dependencies:
  ed(1), cscope, grep, awk

# C code navigation (using cscope)

Cscope provides 10 categories of searches. I provide convenience scripts for a few.
They all follow the general scheme of `<cmd> <symbol/search> <grep>`.

The `grep` can be helpful in limiting the search where duplicate symbols are used
in different cases.

In all cases it is best to execute these commands from the top level of a code tree
as cscope will re-generate an index of all source code from current directory down.

- `definition <function name> <optional grep expression for limiting results>`
  If one definition is found then the content of that function will be printed out.
  FIXME: doesn't work well for typedefs and enums because the symbol is AFTER the content.
- `symbol <search> <grep filter>`
  show file and line number where a symbol is used
- `called <search> <grep filter>`
  where is the function called?
- `assigns <search> <grep filter>`
  where are assignments to this symbol?
- `calling <search> <grep filter>`
  where is this function called?
- `including <search> <grep filter>`
  which files include this file?
